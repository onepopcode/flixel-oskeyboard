package flixel.oskeyboard;

import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.util.FlxColor;
import flixel.text.FlxText;

class OSKey extends FlxSpriteGroup
{
	private static inline var KEY_WIDTH:Int = 80;
	private static inline var KEY_HEIGHT:Int = 90;
	public var char:String;
	private var keycode:Int;
	public function new(Character:String):Void
	{
		super();
		char = Character;
		// Create background
		var bg:FlxSprite = new FlxSprite(0,0);
		bg.makeGraphic(KEY_WIDTH,KEY_HEIGHT,0xffcccccc);
		add(bg);
		// Create text and center it
		var txt:FlxText = new FlxText(0,0,60,Character,40,true);
		txt.alignment = "center";
		add(txt);
	}
}
