package flixel.oskeyboard;

import flixel.group.FlxSpriteGroup;
import flixel.oskeyboard.OSKey;
import flixel.FlxSprite;
import flixel.plugin.MouseEventManager;
import flash.events.EventDispatcher;
import flash.events.KeyboardEvent;
import flash.ui.Keyboard;
import flash.events.IEventDispatcher;
import flixel.addons.ui.FlxInputText;
import flixel.interfaces.IFlxInput;

// import flash.display.BitmapData;

// @:bitmap("assets/images/layout.png")
// private class LayoutGraphic extends BitmapData {}

class OSKeyboard extends FlxSpriteGroup implements IFlxInput
{
	private var keys:Array<String>;
	private var tf:FlxInputText;
	public function new(textField:FlxInputText):Void
	{
		super();
		
		keys = ['q','w','e','r','t','y'];
		tf = textField;

		for(i in 0...keys.length)
		{
			var k:OSKey = new OSKey(keys[i]);
			k.x = (k.width + 5) * i;
			MouseEventManager.add(k,keyTouched);
			add(k);
		}

		// var k:OSKey = new OSKey("q");
		// add(k);

		// var sp:FlxSprite = new FlxSprite(0,0,LayoutGraphic);
		// add(sp);
	}
	private function keyTouched(Key:OSKey):Void
	{
		var ed:EventDispatcher = new EventDispatcher();
		ed.dispatchEvent(new KeyboardEvent( KeyboardEvent.KEY_DOWN , true , true , flash.ui.Keyboard.Q , Keyboard.Q));
	}

	public override function update():Void
	{
		
	}
}